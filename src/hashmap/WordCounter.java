package hashmap;
import java.util.HashMap;


public class WordCounter {
	public String[] list;
	public String message;
	public HashMap<String,Integer> wordCount;


	public WordCounter(String message){
		list = message.split(" ");
		wordCount = new HashMap<String,Integer>();
	}

	public void count(){
		for (int i = 0; i < list.length; i++) {
			if(!wordCount.containsKey(list[i])){
				wordCount.put(list[i], 1);
			}else{
				wordCount.put(list[i], wordCount.get(list[i]) + 1);
			}
		}
	}

	public int hasWord(String word){
		int count = 0;
		for (Object obj : wordCount.keySet()){
			if(obj.equals(word)){
				count = wordCount.get(obj);
			}

		}
		return count;
	}

	public static void main(String[] args) {
		WordCounter counter = new WordCounter("here is the root of the root and the bud of the bud");
		counter.count();
		System.out.println(counter.hasWord("root"));
		System.out.println(counter.hasWord("leaf"));
	}
}
