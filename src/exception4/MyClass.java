package exception4;

public class MyClass {

	public void methX() throws DataException{
		throw new DataException();
	}
	public void methY() {
//		throw new FormatException();
	}

	public static void main(String[] args) {
		try{
			MyClass c = new MyClass();
			System.out.println("A");
			c.methX();
			System.out.println("B");
			c.methY();
			System.out.println("C");
			return;
		}catch (DataException e){
			System.out.println("D");
		}catch (FormatException e){
			System.out.println("E");
		}finally {
			System.out.println("F");
		}
		System.out.println("G");
	}
}
